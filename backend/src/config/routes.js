const express = require("express")
const auth = require("./auth")

//Recupera o servidor (como parâmetro) e retorna as rotas
module.exports = function(server) {
    //Dois contextos: 
    // - Contexto não autenticado - apenas consultas
    // - Contexto autenticado - usuário logado e com possibilidade de manipulação

    const protectedApi = express.Router()
    server.use("/api", protectedApi)

    protectedApi.use(auth)

    /**
 * 
 */

    const BillingCycle = require("../api/billingCycle/billingCycleService")
    BillingCycle.register(protectedApi, "/billingCycles")


    /**
 * Rotas Abertas
 */

    const openApi = express.Router()
    server.use("/oapi", openApi)

    const AuthService = require("../api/user/authService")
    openApi.post("/login", AuthService.login)
    openApi.post("/signup", AuthService.signup)
    openApi.post("/validateToken",AuthService.validateToken)
}