const restful = require("node-restful")


const mongoose = restful.mongoose

const userSchema = new mongoose.Schema({
    personName: {type: String, required: true},
    personSurName: {type: String, required: true},
    userName: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, min: 6, max: 12, required: true}

})

module.exports = restful.model("User", userSchema)
