const _=require("lodash")
const jwt = require("jsonwebtoken")
const bcrypt = require("bcrypt")
const User = require("./user")
const env = require("../../.env")

const emailRegex = /\S+@\S+\.\S+/
const passwordRegex = /((?=.*\d)(?=.*[1-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})/

const sendErrorsFromDB = (res, dbErrors) => {
    const errors = []
    _.forIn(dbErrors.errors,error => errors.push(error.message))
    return res.status(400).json({errors})
}

const login = (req,res,next) => {
    const identity = req.body.identity || ""
    const password = req.body.password || ""

    //    User.findOne({$or: [{email: identity}, {userName: identity}]},(err,user)=>{
    User.findOne({email: identity},(err,user)=>{
        if(err){
            return sendErrorsFromDB(res,err)
        }
        else if (user && bcrypt.compareSync(password,user.password)){
            const token = jwt.sign({data: user},env.authSecret,{expiresIn: "24h"})
            const {personName, personSurName, userName, email} = user
            res.json({personName, personSurName, userName, email, token})
        } else {
            return res.status(400).send({errors: ["Usuário/Senha Inválidos"]})
        }

    })
}

const validateToken = (req, res, next) => {
    const token = req.body.token || ""
    jwt.verify(token,env.authSecret, function(err,decoded){
        return res.status(200).send({valid:!err})
    })
}

const signup = (req,res,next)=>{
    const userName = req.body.userName || ""
    const personName = req.body.personName || ""
    const personSurName = req.body.personSurName ||""
    const email = req.body.email || ""
    const password = req.body.password || ""
    const confirmPassword = req.body.confirmPassword || ""

    if(!email.match(emailRegex)){
        return res.status(400).send({errors: ["O e-mail informado está inválido"]})
    }

    if(!password.match(passwordRegex)){
        return res.status(400).send({
            errors:[
                "Senha precisa ter: Uma letra maiúscula, uma letra minúscula, um número um caractere especial (@#$%) e ter entre 6 e 20 caracteres "
            ]
        })
    }

    const salt = bcrypt.genSaltSync()
    const passwordHash = bcrypt.hashSync(password,salt)
    if(!bcrypt.compareSync(confirmPassword, passwordHash)){
        return res.status(400).send({errors:["Senhas não conferem."]})
    }

    //User.findOne({$or: [{email}, {userName}]},(err,user) => {
    User.findOne({email},(err,user) => {
        if(err) {
            return sendErrorsFromDB(res,err)
        } else if(user){
            return res.status(400).send({errors:["Usuário já cadastrado"]})
        } else {
            const newUser = new User({personName, personSurName, userName, email, password: passwordHash})
            newUser.save(err=>{
                if(err){
                    return sendErrorsFromDB(res,err)
                } else {
                    login(req,res,next)
                }
            })
        }
    })

}

module.exports = {login, signup, validateToken}