const billingCycle = require("./billingCycle")

const errorHandler = require("../common/errorHandler")
//Mapeia os serviços nas requisiçoes
billingCycle.methods(["get","post","put","delete"])
//Indica que os validadores devem ser usados na atualização e que o novo objeto, atualizado, deve ser retornado
billingCycle.updateOptions({new: true, runValidators: true})

billingCycle.after("post",errorHandler).after("put",errorHandler)

billingCycle.route("count",(req,res,next)=>{
    billingCycle.count((error,value)=>{
        if(error){
            res.status(500).json({errors:[error]})
        } else {
            res.json({value})
        }
    })
})

billingCycle.route('summary',(req,res,next)=>{
    console.log("Summary chamado")
    billingCycle.aggregate({
        $project: {credit: {$sum: "$credits.i_value"}, debit:{$sum: "$debts.i_value"}},
    },{
        $group: {_id:null, credit: {$sum: "$credit"}, debt: {$sum: "$debit"}}
    },{
        $project:{_id:0,credit:1,debt:1}
    },(error,result)=>{
        console.log("Deu erro!")
        if(error){
            res.status(500).json({errors: [error]})
        } else {
            res.json(result[0]||{credit:0, debit:0})
        }
    })
})

module.exports = billingCycle