const restful = require ("node-restful")
const mongoose = restful.mongoose


//Monta o esquema de mapeamento objeto ao banco para automatizar o crud

const creditSchema = new mongoose.Schema({
    i_name: {type: String, required: true},
    i_value: {type: Number, min: 0, required: true}
})

const debtSchema = new mongoose.Schema({
    i_name:{type: String, required: true},
    i_value: {type:Number, min:0, required: true},
    status: {type: String, required: false, uppercase:true, enum:["PAGO", "PENDENTE", "AGENDADO"]}
})

const billingCycleSchema = new mongoose.Schema({
    name: {type: String, required: true},
    month: {type: Number, min:1, max:12, required:true},
    year: {type:Number, min:1970, required: true},
    credits:[creditSchema],
    debts:[debtSchema]
})


module.exports = restful.model("billingcycles", billingCycleSchema)