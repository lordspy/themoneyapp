const webpack = require("webpack")
const ExtractTextPlugin = require("extract-text-webpack-plugin")

module.exports = {
    entry: "./src/index.jsx",
    output: {
        path: __dirname + "/public",
        filename: "app.js"
    },
    module: {
        loaders: [{
            test: /.js[x]?$/,
            loader: "babel-loader",
            exclude: /node_modules/,
            query: {
                presets: ["es2015","react"],
                plugins:["transform-object-rest-spread"]
            }
        },{
            test:/\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: "css-loader"
            })
        },{
            test: /\.woff|.woff2|.ttf|.eot|.svg|.png|.jpg*.*$/,
            loader: "file-loader"
        }
        ]
    },
    resolve: {
        extensions: [".js", ".jsx"],
        alias: {
            modules: __dirname + "/node_modules",
            jquery: "modules/jquery/dist/jquery.min.js",
            bootstrap: "modules/admin-lte/bootstrap/js/bootstrap.js"
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            $:"jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
        new ExtractTextPlugin("app.css")
    ],
    devServer: {
        port: 8080,
        contentBase: "./public"
    }
};