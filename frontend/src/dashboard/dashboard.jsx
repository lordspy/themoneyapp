import React, {Component} from "react"

//Busca os conectores para relacionar o componente e o estado
import {connect} from "react-redux"

//Importa os métodos para relacionar a ação com o estado (store)
import {bindActionCreators} from "redux"

import ContentHeader from "../common/template/contentHeader"
import Content from "../common/template/content"
import ValueBox from "../common/widget/valueBox"
import Row from "../common/layout/row"
import {getSummary} from "./dashboardActions"

class Dashboard extends Component {

    componentWillMount(){
        this.props.getSummary()
    }
    render(){
        //Possivel devido à lib summary
        const {credit, debt} = this.props.summary
        return (
            <div>
                <ContentHeader title="Dashboard" small="1.0"/>
                <Content>
                    <Row>
                        <ValueBox cols="12 4" color="green" icon="bank" value={`R$ ${credit}`} text="Total de Créditos"/>
                        <ValueBox cols="12 4" color="red" icon="credit-card" value={`R$ ${debt}`} text="Total de Débitos"/>
                        <ValueBox cols="12 4" color="blue" icon="money" value={`R$ ${credit-debt}`} text="Valor Consolidado"/>
                    
                    </Row>
                </Content>
            </div>
        )
    }
}

//Define uma função para mapear os props com o estado

const mapStateToProps = (state) => {
    return {
        summary: state.dashboard.summary
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({getSummary},dispatch)

// conecta o componente com o estado através da função de mapeamento
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)

//export default Dashboard