import React, {Component} from "react"

//Transforma num container (liga ao estado do redux) junto com as ações das tabs
import {bindActionCreators} from "redux"
import {connect} from "react-redux"

//As actionsdo billingCycle estão sendo importadas para configurar os submits dos formulários
import {create, update, remove, init} from "./billingCycleActions"


import ContentHeader from "../common/template/contentHeader"
import Content from "../common/template/content"
import Tabs from "../common/tab/tabs"
import TabsHeader from "../common/tab/tabsheader"
import TabHeader from "../common/tab/tabheader"
import TabsContent from "../common/tab/tabscontent"
import TabContent from "../common/tab/tabcontent"

import BillingCyclesList from "./billingCycleList"
import Form from "./billingCycleForm"


class BillingCycle extends Component{



    componentWillMount(){
        this.props.init()
        
    }

    render() {
        console.log("Entrou")
        return (
        <div>
            <ContentHeader title="Ciclos de Pagamento" small="Cadastro"/>
            <Content>
                <Tabs>
                    <TabsHeader>
                        <TabHeader label="Lista" icon="bars" target="tabList"/>
                        <TabHeader label="Incluir" icon="plus" target="tabCreate"/>
                        <TabHeader label="Alterar" icon="pencil" target="tabUpdate"/>
                        <TabHeader label="Excluir" icon="trash-0" target="tabDelete"/>
                    </TabsHeader>
                    <TabsContent>
                        <TabContent id="tabList"><BillingCyclesList/></TabContent>
                        <TabContent id="tabCreate"><Form onSubmit={this.props.create} submitClass="primary" submitLabel="Incluir"/></TabContent>
                        <TabContent id="tabUpdate"><Form onSubmit={this.props.update} submitClass="info" submitLabel="Alterar"/></TabContent>
                        <TabContent id="tabDelete"><Form onSubmit={this.props.remove} readOnly={true} submitClass="danger" submitLabel="Excluir"/></TabContent>
                    </TabsContent>
                </Tabs>
            </Content>
        </div>
    )
    }
}



const mapDispatchToProps = (dispatch) => bindActionCreators({create, update, remove, init},dispatch)
export default connect(null,mapDispatchToProps)(BillingCycle)