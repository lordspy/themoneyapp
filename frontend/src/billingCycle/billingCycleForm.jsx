import React, { Component } from "react"
import {reduxForm, Field, formValueSelector} from "redux-form"

import {bindActionCreators} from "redux"
import {connect} from "react-redux"

import {init} from "./billingCycleActions"

import LabelAndInput from "../common/form/labelAndInput"
import ItemList from "./itemList"
import Summary from "./summary"
//reduxForm é como a connect do redux geral, Field é uma tag para campos do formulário
class BillingCycleForm extends Component {

    calculateSummary(){
        const sum=(t,v)=>t+v
        return {
            sumOfCredits: this.props.credits.map(c=>+c.value||0).reduce(sum),
            sumOfDebts: this.props.debts.map(d=>+d.value||0).reduce(sum)
        }
    }

    render(){
        const {handleSubmit, readOnly, credits, debts} = this.props
        const {sumOfCredits, sumOfDebts} = this.calculateSummary()
        return (

            <form role="form" onSubmit={handleSubmit}>
                <div className="box-body">
                    <Field name="name" readOnly={readOnly} component={LabelAndInput} label="Nome" cols="12 4" placeholder="Informe o nome"/>
                    <Field name="month"  readOnly={readOnly} component={LabelAndInput} label="Mês" type="number" cols="12 4" placeholder="Informe o mês"/>
                    <Field name="year"  readOnly={readOnly} component={LabelAndInput} label="Ano" cols="12 4" type="number" placeholder="Informe o Ano"/>
                    <Summary credit={sumOfCredits} debt={sumOfDebts}/> 
                    <ItemList cols="12 6" readOnly={readOnly} list={credits} field="credits" legend="Créditos"/>
                    <ItemList cols="12 6" readOnly={readOnly} list={debts} field="debts" legend="Débitos" showStatus={true}/>
                </div>
                <div className="box-footer">
                    <button type="submit" className={`btn btn-${this.props.submitClass}`}>
                        {this.props.submitLabel}
                    </button>
                    <button type="button" className="btn btn-default" onClick={this.props.init}>Cancelar</button>
                </div>
            </form>
        )
    }
}

//destroyonunmount Impede a destruição fo formulário quando o componente se torna inativo (preserva os dados para outras operações)
BillingCycleForm = reduxForm({form: "billingCycleForm", destroyOnUnmount: false})(BillingCycleForm)
const selector = formValueSelector("billingCycleForm")

const mapStateToProps = (state) => ({
    credits: selector(state,"credits"),
    debts: selector(state,"debts")
})

const mapDispatchToProps = (dispatch) => bindActionCreators({init}, dispatch)
export default connect(mapStateToProps,mapDispatchToProps)(BillingCycleForm)