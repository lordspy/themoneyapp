import React, {Component} from "react"
import {bindActionCreators} from "redux"
import {connect} from "react-redux"
import {getBillingCycles, showUpdate, showDelete} from "./billingCycleActions"

class BillingCycleList extends Component {
    componentWillMount(){
        this.props.getBillingCycles()
        
    }

    renderRows(){
        const list=this.props.list||[]
        //mapeia pra cada elemento da lista a função cujo retorno é uma linha da tabela.
        //react não aceita foreach(?)
        return list.map(bc => (
            <tr key={bc._id}>
                <td>{bc.name}</td>
                <td>{bc.month}</td>
                <td>{bc.year}</td>
                <td><button className="btn btn-warning" onClick={()=>this.props.showUpdate(bc)}><i className="fa fa-pencil"></i></button>
                    <button className="btn btn-danger" onClick={()=>this.props.showDelete(bc)}><i className="fa fa-trash-o"></i></button></td>
            </tr>
        ))

    }
    render(){
        console.log(this.props.list)
        return (//Não adianta colocar foreach no React.
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>nome</th>
                            <th>Mês</th>
                            <th>Ano</th>
                            
                            <th className="table-actions">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderRows()}
                    </tbody>
                </table>

            </div>
        )
    }

}
const mapStateToProps = (state) => ({list: state.billingCycle.list}
)
const mapDispatchToProps = (dispatch) => bindActionCreators({getBillingCycles, showUpdate, showDelete},dispatch)

export default connect(mapStateToProps,mapDispatchToProps)(BillingCycleList)
