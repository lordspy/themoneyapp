import "../common/template/dependencies"

import React from "react"

import Header from "../common/template/header"
import SideBar from "../common/template/sidebar"
import Footer from "../common/template/footer"
import Messages from "../common/msg/messages"

export default props => (
    //O content-wrapper é a tela de operação. O router é o que carrega assincronamente o conteúdo para essa tela.
    <div className="wrapper">
        <Header />
        <SideBar />
        <div className="content-wrapper">
            {props.children}
        </div>
        <Footer />
        <Messages/>
    </div>
)