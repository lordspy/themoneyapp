import React from "react"
import {Route, Redirect, HashRouter as Router, Switch} from "react-router-dom"
import Dashboard from "../dashboard/dashboard"
import BillingCycle from "../billingCycle/billingCycle"
import Doorman from "./doorMan"
export default props => (
    <Router>
        <Doorman>
            <div>
                <Switch>
                    <Route path="billingCycles" component={BillingCycle}/>
                    <Route component={Dashboard}/>
                </Switch>
            </div>
        </Doorman>
    </Router>
)