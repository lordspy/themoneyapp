import React,{Component} from "react"
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {reduxForm, Field} from "redux-form"


import {login,signup} from "./authActions"

import Row from "../common/layout/row"
import Grid from "../common/layout/grid"
import If from "../common/operator/if"
import Messages from "../common/msg/messages"
import Input from "../common/form/inputAuth"

class Auth extends Component {
    constructor(props){
        super(props)
        this.state={loginMode:true}
    }

    changeMode(){
        this.setState({loginMode: !this.state.loginMode})
    }

    onSubmit(values){
        const {login,signup} = this.props
        this.state.loginMode?login(values):signup(values)
    }

    render(){
        const {loginMode} = this.state
        //handleSubmit é um método provido pelo redux-form
        const {handleSubmit} = this.props
        return(
            <div className="login-box">
                <div className="login-logo"><b>Minha</b> Grana</div>
                <div className="login-box-body">
                    <p className="login-box-msg">Bem Vindo</p>
                    <form onSubmit={handleSubmit(v=>this.onSubmit(v))}>
                        <Field component={Input} type="input" name="personName" placeholder="Primeiro Nome" icon="user" hide={loginMode}/>
                        <Field component={Input} type="input" name="personSurName" placeholder="Sobrenome" icon="user" hide={loginMode}/>
                        <Field component={Input} type="input" name="userName" placeholder="Nome de Usuário" icon="user" hide={loginMode}/>
                        <Field component={Input} type="email" name="identity" placeholder="e-mail ou userName" icon="asterisk" hide={!loginMode}/>
                        <Field component={Input} type="email" name="email" placeholder="e-mail" icon="envelope" hide={loginMode}/>
                        <Field component={Input} type="password" name="password" placeholder="Senha" icon="lock"/>
                        <Field component={Input} type="password" name="confirmPassword" placeholder="confirmar Senha" icon="lock" hide={loginMode}/>
                        <Row>
                            <Grid cols="4">
                                <button type="submit" className="btn btn-primary btn-block btn-flat">
                                    {loginMode?"Entrar":"Registrar"}
                                </button>
                            </Grid>
                        </Row>
                    </form>
                    <br/>
                    <a href="javascript:;" onClick={()=>this.changeMode()}>
                        {loginMode?"Novo Usuário? Registrar aqui!":"Já é cadastrado? Entrar aqui!"}
                    </a>
                </div>
                <Messages/>
            </div>
        )
    }

}

Auth = reduxForm({form: "authForm"})(Auth)
const mapDispatchToProps = (dispatch) => bindActionCreators({login,signup},dispatch)
export default connect(null,mapDispatchToProps)(Auth)