import React from "react"

export default props => (
    <footer className="main-footer">
        <strong>
            Copyright &copy; 2017
            <a href="http://hiperlogic.com.br" target="_blank">Hiperespaço Logicware</a>
        </strong>
    </footer>
)