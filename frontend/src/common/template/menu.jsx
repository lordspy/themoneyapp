import React from "react"
import MenuItem from "./menuItem"
import MenuTree from "./menuTree"
export default props => (
    //Nova versão do Admin-LTE, componente treeview deve estar em um componente tree... (justo)
    <ul className="sidebar-menu tree" data-widget="tree">
        <MenuItem label="Dashboard" icon="dashboard" path="/"/>
        <MenuTree label="Cadastro" icon="edit">
            <MenuItem path="billingCycles"
                label="Ciclos de pagamento" icon="usd"/>
        </MenuTree>
    </ul>
)