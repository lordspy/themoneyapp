import React from "react"

export default props => (
    <li className="treeview">
        <a href="#">
            <i className={`fa fa-${props.icon}`}></i>
            <span>{props.label}</span>
            <span className="fa fa-angle-left pull-right"></span>
        </a>
        <ul className="treeview-menu">
            {props.children}
        </ul>
    </li>
) 